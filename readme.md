# Photo gallery 

### About 

This is just a simple photo gallery with a carousel. You can display as many photos as you wish by giving their url's to the "photos" array.

I have used following technologies to build this project: 

* HTML
* SASS
* JavaScript

## To run this project

```bash
# install dependencies
$ npm install

# run server
$ npm run serve
```

