let photos = [
  "https://i.pinimg.com/originals/9b/42/51/9b42518f70f1d3fae5894e9758a79810.jpg",
  "https://fajnepodroze.pl/wp-content/uploads/2018/07/ocean.jpg",
  "https://www.euractiv.pl/wp-content/uploads/sites/6/2020/05/max-baskakov-OzAeZPNsLXk-unsplash-scaled-e1588503189401-800x450.jpg",
  "https://www.petmd.com/sites/default/files/Acute-Dog-Diarrhea-47066074.jpg",
  "http://meatmyday.pl/wp-content/uploads/2018/11/Gory-Szwajcarii-1024x551.jpg",
  "https://www.alaskawildlife.org/wp-content/uploads/2016/12/moose-face.jpg",
];

const galleryStart = document.querySelector(".gallery");
const dotList = document.querySelector(".navigationDots");
const thumbnails = document.querySelector(".thumbnails");
const display = document.querySelector(".display");
const browser = document.querySelector(".browser");
const selected = document.querySelector(".selectedPhoto");

function renderPhotos(list, className) {
  let i = 0;
  photos.forEach((element) => {
    const li = document.createElement("li");

    const img = document.createElement("img");
    img.src = element;
    img.setAttribute("photoNo", `${i}`);
    img.classList.add(className);

    li.appendChild(img);
    list.appendChild(li);

    i = i + 100;
  });
}

renderPhotos(galleryStart);
renderPhotos(thumbnails, "thumbPhotos");
renderPhotos(selected);

function generateDots() {
  for (i = 0; i < photos.length; i++) {
    const dot = document.createElement("li");
    dot.id = i * 100;
    dot.classList.add("dot");
    dotList.appendChild(dot);
  }
}
generateDots();

const nextBtn = document.querySelector(".next");
const previousBtn = document.querySelector(".previous");

let start = 0;

function next(gallery) {
  if (start < photos.length * 100 - 100) {
    gallery.style.transform = `translateX(-${start + 100}%)`;
    start += 100;
    highlight();
    moveDots();
  } else return;
}

function prev(gallery) {
  if (start >= 100) {
    gallery.style.transform = `translateX(-${start - 100}%)`;
    start -= 100;
    highlight();
    moveDots();
  } else return;
}

const dots = document.querySelectorAll(".dot");
const dotsArray = Array.from(dots);


function moveDots() {
  dotsArray.find((element) => {
    if (element.id == start) {
      element.style.backgroundColor = "rgb(84, 84, 80)";
    } else if (element.id != start) {
      element.style.backgroundColor = "rgb(173, 173, 164)";
    }
  });
}

function highlight() {
  let thumbPhotos = Array.from(document.querySelectorAll(".thumbPhotos"));
  thumbPhotos.find((element) => {
    if (element.getAttribute("photoNo") == start) {
      element.parentElement.style.boxShadow = "1px 2px 12px 3px rgba(166,168,171,1)";
      element.parentElement.style.transform = "scale(1.05)";
    } else if (element.getAttribute("photoNo") != start) {
      element.parentElement.style.boxShadow = "none";
      element.parentElement.style.transform = "scale(1)";
    }
  });
}

nextBtn.addEventListener("click", (event) => next(galleryStart));
previousBtn.addEventListener("click", (event) => prev(galleryStart));

const nextPhoto = document.querySelector(".nextPhoto");
nextPhoto.addEventListener("click", (event) => next(selected));

const prevPhoto = document.querySelector(".previousPhoto");
prevPhoto.addEventListener("click", (event) => prev(selected));

const close = document.querySelector(".close");
close.addEventListener("click", (event) => {
  display.classList.add("none");
  browser.classList.remove("none");
});

let images = Array.from(document.querySelectorAll("img"));

images.forEach((element) => {
  element.addEventListener("click", (event) => {
    browser.classList.add("none");
    display.classList.remove("none");

    const selectedImg = event.target;
    const position = selectedImg.getAttribute("photoNo");
    selected.style.transform = `translateX(-${position}%)`;
    start = parseInt(position);

    highlight();
  });
});
